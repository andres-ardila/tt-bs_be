﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TT_BS_BE.Migrations
{
    public partial class Renamefields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "registerDate",
                table: "User");

            migrationBuilder.DropColumn(
                name: "registerDate",
                table: "Report");

            migrationBuilder.AddColumn<long>(
                name: "registrationDate",
                table: "User",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "registrationDate",
                table: "Report",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "registrationDate",
                table: "User");

            migrationBuilder.DropColumn(
                name: "registrationDate",
                table: "Report");

            migrationBuilder.AddColumn<long>(
                name: "registerDate",
                table: "User",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "registerDate",
                table: "Report",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
