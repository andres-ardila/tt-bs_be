﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TT_BS_BE.Migrations
{
    public partial class Renamefields3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "il",
                table: "User",
                newName: "iL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "iL",
                table: "User",
                newName: "il");
        }
    }
}
