﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TT_BS_BE.Migrations
{
    public partial class Renamefields2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Report",
                table: "Report");

            migrationBuilder.DropColumn(
                name: "Ad",
                table: "User");

            migrationBuilder.DropColumn(
                name: "gL",
                table: "User");

            migrationBuilder.DropColumn(
                name: "zu",
                table: "User");

            migrationBuilder.RenameTable(
                name: "Report",
                newName: "Metrics");

            migrationBuilder.AddColumn<string>(
                name: "Bd",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Eu",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "il",
                table: "User",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Metrics",
                table: "Metrics",
                column: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Metrics",
                table: "Metrics");

            migrationBuilder.DropColumn(
                name: "Bd",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Eu",
                table: "User");

            migrationBuilder.DropColumn(
                name: "il",
                table: "User");

            migrationBuilder.RenameTable(
                name: "Metrics",
                newName: "Report");

            migrationBuilder.AddColumn<string>(
                name: "Ad",
                table: "User",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "gL",
                table: "User",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "zu",
                table: "User",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Metrics",
                table: "MEtrics",
                column: "ID");
        }
    }
}
