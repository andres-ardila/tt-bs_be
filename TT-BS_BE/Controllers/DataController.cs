using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TT_BS_BE.Models;
using TT_BS_BE.Persistance;

namespace TT_BS_BE.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class DataController : ControllerBase
    {
        private readonly ILogger<DataController> _logger;

        public DataController(ILogger<DataController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult> TestOk()
        {
            return Ok();
        }

        [HttpPost]
        [Route("metrics")]
        public async Task<ActionResult> SaveMetrics(JsonElement metrics)
        {
            if (!checkAuth())
                return Unauthorized();

            try
            {
                Metrics objMetrics = new Metrics();
                objMetrics.reports = Regex.Unescape(metrics.ToString());
                objMetrics.registrationDate = DateTimeOffset.Now.ToUnixTimeSeconds();

                MainDBContext mainDbContext = new MainDBContext();
                mainDbContext.Add(objMetrics);
                await mainDbContext.SaveChangesAsync();

                return Created("", null);
            }
            catch (Exception e)
            {
                _logger.LogError("error", e);
                throw;
            }
        }

        [HttpPost]
        [Route("user")]
        public async Task<ActionResult> SaveUser(User user)
        {
            if (!checkAuth())
                return Unauthorized();

            try
            {
                user.registrationDate = DateTimeOffset.Now.ToUnixTimeSeconds();

                MainDBContext mainDbContext = new MainDBContext();
                mainDbContext.Add(user);
                await mainDbContext.SaveChangesAsync();

                return Created("", null);
            }
            catch (Exception e)
            {
                _logger.LogError("error", e);
                throw;
            }
        }

        [HttpGet]
        [Route("user")]
        public async Task<ActionResult<List<User>>> GetAllUsers()
        {
            if (!checkAuth())
                return Unauthorized();

            try
            {
                MainDBContext mainDbContext = new MainDBContext();
                List<User> last = mainDbContext.User.ToList();
                await mainDbContext.SaveChangesAsync();

                return Ok(last);
            }
            catch (Exception e)
            {
                _logger.LogError("error", e);
                throw;
            }
        }

        [HttpGet]
        [Route("user/last")]
        public async Task<ActionResult<User>> GetLastUser()
        {
            if (!checkAuth())
                return Unauthorized();

            try
            {
                MainDBContext mainDbContext = new MainDBContext();
                User lastUser = mainDbContext.User.OrderByDescending(p => p.registrationDate).FirstOrDefault();

                return Ok(lastUser);
            }
            catch (Exception e)
            {
                _logger.LogError("error", e);
                throw;
            }
        }

        [HttpGet]
        [Route("metrics")]
        public async Task<ActionResult<List<Metrics>>> GetAllMetrics()
        {
            if (!checkAuth())
                return Unauthorized();

            try
            {
                MainDBContext mainDbContext = new MainDBContext();
                List<Metrics> last = mainDbContext.Metrics.ToList();
                await mainDbContext.SaveChangesAsync();

                return Ok(last);
            }
            catch (Exception e)
            {
                _logger.LogError("error", e);
                throw;
            }
        }

        [HttpGet]
        [Route("metrics/last")]
        public async Task<ActionResult<User>> GetLastMetrics()
        {
            if (!checkAuth())
                return Unauthorized();

            try
            {
                MainDBContext mainDbContext = new MainDBContext();
                Metrics lastMetrics = mainDbContext.Metrics.OrderByDescending(p => p.registrationDate).FirstOrDefault();

                return Ok(lastMetrics);
            }
            catch (Exception e)
            {
                _logger.LogError("error", e);
                throw;
            }
        }

        private bool checkAuth()
        {
            string auth = Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value.FirstOrDefault();

            return (auth != null);
        }
    }
}