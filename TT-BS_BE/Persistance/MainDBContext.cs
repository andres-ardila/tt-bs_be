using System;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using TT_BS_BE.Models;

namespace TT_BS_BE.Persistance
{
    public class MainDBContext : DbContext
    {
        public MainDBContext(DbContextOptions<MainDBContext> options)
            : base(options)
        {
        }

        public MainDBContext()
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Metrics> Metrics { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(
                    "Server=prestashop17.clecwqycf6tt.us-east-2.rds.amazonaws.com;Database=tt_bs;User=tt_bs;Password=L`k/^mx/,VA5<*,g;",
                    mySqlOptions => mySqlOptions
                        .ServerVersion(new Version(5, 7, 24), ServerType.MySql));
            }
        }
    }
}