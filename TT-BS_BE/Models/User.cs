using System.ComponentModel.DataAnnotations;
using System.Numerics;

namespace TT_BS_BE.Models
{
    public class User
    {
        [Key]
        public long ID { get; set; }
        public string Bd { get; set; }
        public string Eu { get; set; }
        public string iL { get; set; }
        public long registrationDate { get; set; }
    }
}