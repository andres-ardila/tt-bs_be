using System.ComponentModel.DataAnnotations;
using System.Numerics;

namespace TT_BS_BE.Models
{
    public class Metrics
    {
        [Key]
        public long ID { get; set; }
        public string reports { get; set; }
        public long registrationDate { get; set; }
    }
}